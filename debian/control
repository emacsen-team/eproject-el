Source: eproject-el
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13)
             , dh-elpa
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/emacsen-team/eproject-el
Vcs-Git: https://salsa.debian.org/emacsen-team/eproject-el.git
Homepage: https://github.com/jrockway/eproject

Package: elpa-eproject
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs
Enhances: emacs
Description: assign files to Emacs projects, programmatically
 Eproject is a minor-mode that allows the grouping of related files
 as projects.  It aims to be as unobtrusive as possible -- no new
 files are created (or required to exist) on disk, and buffers that
 are not a member of a project are not affected in any way.  Where
 'auto-mode-alist activates MODE for FILE-REGEX, Eproject
 activates PROJECT-TYPE for FILES-IN-DIRECTORY, when
 PROJECT-TYPE-DEFINITION is true.  It also creates a customisable
 hook that is run whenever a file belonging to a project is visited.
